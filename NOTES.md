Notes
=====

A log of what I've tried, what worked, what didn't. I keep it around because it could be helpful to reconstruct how to make any of this work.

# Background

I'm trying to do some development work on the [XNAT Container Service](https://bitbucket.org/xnatdev/container-service) to enable it to launch tasks on Kubernetes. We were hoping to do that development locally, using a fully local cluster, with XNAT running in it and the container service launching its tasks from inside.

To do that, I tested whether I could use skaffold to deploy an XNAT with a development version of the container service plugin jar.

# Helm setup
I started with a known XNAT helm chart from the [AIS/charts](https://github.com/Australian-Imaging-Service/charts) repo. That chart defines its services using an XNAT image created in the [AIS/xnat-build](https://github.com/Australian-Imaging-Service/xnat-build) repo.

For my testing I wanted to leave this chart as-is to the extent possible in order to isolate variables.

## Getting Helm Chart Running
First task is to deploy helm chart as-is with no skaffold.

Started with base chart on current `main` branch, tag `xnat-1.1.5`.

I had to override some of the chart's values. I put these into the `values.yaml` file in this repo.

Ran this command to get the chart installed:
```
helm repo add ais https://australian-imaging-service.github.io/charts
helm repo update
```

Then ran this command to start the chart:
```
helm upgrade xnat ais/xnat --install --values values.yaml --namespace xnat --create-namespace
```
(Note that the `values.yaml` there refers to the value overrides file in this repo. So if you're in some other directory, use the absolute path.)

Watch the progress creating all the stuff with
```
watch kubectl -nxnat-demo get all,pv,pvc
```

Watch XNAT startup logs
```
kubectl logs --follow xnat-xnat-web-0 -c xnat-web -nxnat
```

Can also be helpful to see the events
```
kubectl get event -nxnat
```

When things inevitably go wrong and I have to remove/reset everything, I clean up with
```
helm uninstall xnat -nxnat && kubectl delete pvc --all -nxnat && kubectl delete pv --all
```

## Explanations of value overrides

Reduced size of the postgres volume by setting
```yaml
postgresql:
  persistence:
    size: 500Mi
```

The volumes were a problem. There are two types of volumes defined in the values and chart:
some like archive and prearchive under the `volumes` key and others like cache under the `persistence` key.
The ones under `cache` would create a PVC and autoprovision a corresponding PV to go with it, but the ones
under `volumes` would only create a PVC, not a PV. I couldn't figure out what was going wrong there.
In the end I had to modify the `accessMode` on the `volumes` to `ReadWriteOnce`, and they were able to dynamically provision.

Also reduced size of the archive, prearchive, and cache volumes.

I was having issues with the startup time taking too long which would result in the app getting killed by kubernetes, so I extended the startup probe time.

Also related to startup time, even with the extended startup probe times the app would not get started. I think that's because it didn't have enough memory, so I bumped up the resource requests and limits.

## Results 2023-05-25
Tried 1 CPU and 8Gi memory. We finally got into database initialization. Huzzah!

Database initialization seems to have succeeded.

We get what looks like a server startup success message but then everything immediately stops.

```
...
25-May-2023 14:34:44.971 INFO [main] org.apache.catalina.startup.HostConfig.deployDirectory Deploying web application directory [/usr/local/tomcat/webapps/ROOT]
25-May-2023 14:35:17.402 INFO [main] org.apache.jasper.servlet.TldScanner.scanJars At least one JAR was scanned for TLDs yet contained no TLDs. Enable debug logging for this logger for a complete list of JARs that were scanned but no TLDs were found in them. Skipping unneeded JARs during scanning can improve startup time and JSP compilation time.
25-May-2023 14:38:12.430 WARNING [main] org.apache.catalina.util.SessionIdGeneratorBase.createSecureRandom Creation of SecureRandom instance for session ID generation using [SHA1PRNG] took [173] milliseconds.
SOURCE: /usr/local/tomcat/webapps/ROOT/
===========================
New Database -- BEGINNING Initialization
===========================
...
===========================
Database initialization complete.
===========================
2023-05-25 14:39:48,741 [main] ERROR org.apache.axis.configuration.EngineConfigurationFactoryServlet - Unable to find config file.  Creating new servlet engine config file: /WEB-INF/server-config.wsdd
2023-05-25 14:39:49,410 [main] ERROR org.apache.axis.configuration.EngineConfigurationFactoryServlet - Unable to find config file.  Creating new servlet engine config file: /WEB-INF/server-config.wsdd
25-May-2023 14:39:49.543 INFO [main] org.apache.catalina.startup.HostConfig.deployDirectory Deployment of web application directory [/usr/local/tomcat/webapps/ROOT] has finished in [304,569] ms
25-May-2023 14:39:49.561 INFO [main] org.apache.coyote.AbstractProtocol.start Starting ProtocolHandler ["http-nio-8080"]
25-May-2023 14:39:49.651 INFO [main] org.apache.catalina.startup.Catalina.start Server startup in [304849] milliseconds
25-May-2023 14:40:49.186 INFO [Thread-40] org.apache.coyote.AbstractProtocol.pause Pausing ProtocolHandler ["http-nio-8080"]
25-May-2023 14:40:49.289 INFO [Thread-40] org.apache.catalina.core.StandardService.stopInternal Stopping service [Catalina]
25-May-2023 14:40:54.662 WARNING [Thread-40] org.apache.catalina.loader.WebappClassLoaderBase.clearReferencesThreads The web application [ROOT] appears to have started a thread named [DefaultQuartzScheduler_Worker-1] but has failed to stop it. This is very likely to create a memory leak. Stack trace of thread:
 java.lang.Object.wait(Native Method)
 org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:568)
25-May-2023 14:40:54.671 WARNING [Thread-40] org.apache.catalina.loader.WebappClassLoaderBase.clearReferencesThreads The web application [ROOT] appears to have started a thread named [DefaultQuartzScheduler_Worker-2] but has failed to stop it. This is very likely to create a memory leak. Stack trace of thread:
 java.lang.Object.wait(Native Method)
 org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:568)
25-May-2023 14:40:54.675 WARNING [Thread-40] org.apache.catalina.loader.WebappClassLoaderBase.clearReferencesThreads The web application [ROOT] appears to have started a thread named [DefaultQuartzScheduler_Worker-3] but has failed to stop it. This is very likely to create a memory leak. Stack trace of thread:
 java.lang.Object.wait(Native Method)
 org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:568)
25-May-2023 14:40:54.680 WARNING [Thread-40] org.apache.catalina.loader.WebappClassLoaderBase.clearReferencesThreads The web application [ROOT] appears to have started a thread named [DefaultQuartzScheduler_Worker-4] but has failed to stop it. This is very likely to create a memory leak. Stack trace of thread:
 java.lang.Object.wait(Native Method)
 org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:568)
25-May-2023 14:40:54.683 WARNING [Thread-40] org.apache.catalina.loader.WebappClassLoaderBase.clearReferencesThreads The web application [ROOT] appears to have started a thread named [DefaultQuartzScheduler_Worker-5] but has failed to stop it. This is very likely to create a memory leak. Stack trace of thread:
 java.lang.Object.wait(Native Method)
 org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:568)
25-May-2023 14:40:54.686 WARNING [Thread-40] org.apache.catalina.loader.WebappClassLoaderBase.clearReferencesThreads The web application [ROOT] appears to have started a thread named [DefaultQuartzScheduler_Worker-6] but has failed to stop it. This is very likely to create a memory leak. Stack trace of thread:
 java.lang.Object.wait(Native Method)
 org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:568)
25-May-2023 14:40:54.689 WARNING [Thread-40] org.apache.catalina.loader.WebappClassLoaderBase.clearReferencesThreads The web application [ROOT] appears to have started a thread named [DefaultQuartzScheduler_Worker-7] but has failed to stop it. This is very likely to create a memory leak. Stack trace of thread:
 java.lang.Object.wait(Native Method)
 org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:568)
25-May-2023 14:40:54.692 WARNING [Thread-40] org.apache.catalina.loader.WebappClassLoaderBase.clearReferencesThreads The web application [ROOT] appears to have started a thread named [DefaultQuartzScheduler_Worker-8] but has failed to stop it. This is very likely to create a memory leak. Stack trace of thread:
 java.lang.Object.wait(Native Method)
 org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:568)
25-May-2023 14:40:54.695 WARNING [Thread-40] org.apache.catalina.loader.WebappClassLoaderBase.clearReferencesThreads The web application [ROOT] appears to have started a thread named [DefaultQuartzScheduler_Worker-9] but has failed to stop it. This is very likely to create a memory leak. Stack trace of thread:
 java.lang.Object.wait(Native Method)
 org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:568)
25-May-2023 14:40:54.698 WARNING [Thread-40] org.apache.catalina.loader.WebappClassLoaderBase.clearReferencesThreads The web application [ROOT] appears to have started a thread named [DefaultQuartzScheduler_Worker-10] but has failed to stop it. This is very likely to create a memory leak. Stack trace of thread:
 java.lang.Object.wait(Native Method)
 org.quartz.simpl.SimpleThreadPool$WorkerThread.run(SimpleThreadPool.java:568)
25-May-2023 14:40:54.705 WARNING [Thread-40] org.apache.catalina.loader.WebappClassLoaderBase.clearReferencesThreads The web application [ROOT] appears to have started a thread named [Timer-0] but has failed to stop it. This is very likely to create a memory leak. Stack trace of thread:
 java.lang.Object.wait(Native Method)
 java.util.TimerThread.mainLoop(Timer.java:552)
 java.util.TimerThread.run(Timer.java:505)
25-May-2023 14:40:54.713 WARNING [Thread-40] org.apache.catalina.loader.WebappClassLoaderBase.clearReferencesThreads The web application [ROOT] appears to have started a thread named [threadPoolExecutorFactoryBean-1] but has failed to stop it. This is very likely to create a memory leak. Stack trace of thread:
 java.net.PlainSocketImpl.socketAccept(Native Method)
 java.net.AbstractPlainSocketImpl.accept(AbstractPlainSocketImpl.java:409)
 java.net.ServerSocket.implAccept(ServerSocket.java:560)
 java.net.ServerSocket.accept(ServerSocket.java:528)
 org.dcm4che2.net.NetworkConnection$1.run(NetworkConnection.java:635)
 java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)
 java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
 java.lang.Thread.run(Thread.java:750)
25-May-2023 14:40:54.716 WARNING [Thread-40] org.apache.catalina.loader.WebappClassLoaderBase.clearReferencesThreads The web application [ROOT] appears to have started a thread named [ActiveMQ BrokerService[activeMQBroker] Task-8] but has failed to stop it. This is very likely to create a memory leak. Stack trace of thread:
 sun.misc.Unsafe.park(Native Method)
 java.util.concurrent.locks.LockSupport.parkNanos(LockSupport.java:215)
 java.util.concurrent.SynchronousQueue$TransferStack.awaitFulfill(SynchronousQueue.java:460)
 java.util.concurrent.SynchronousQueue$TransferStack.transfer(SynchronousQueue.java:362)
 java.util.concurrent.SynchronousQueue.poll(SynchronousQueue.java:941)
 java.util.concurrent.ThreadPoolExecutor.getTask(ThreadPoolExecutor.java:1073)
 java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1134)
 java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
 java.lang.Thread.run(Thread.java:750)
25-May-2023 14:40:54.719 WARNING [Thread-40] org.apache.catalina.loader.WebappClassLoaderBase.clearReferencesThreads The web application [ROOT] appears to have started a thread named [DefaultQuartzScheduler_QuartzSchedulerThread] but has failed to stop it. This is very likely to create a memory leak. Stack trace of thread:
 java.lang.Object.wait(Native Method)
 org.quartz.core.QuartzSchedulerThread.run(QuartzSchedulerThread.java:253)
25-May-2023 14:40:54.725 SEVERE [Thread-40] org.apache.catalina.loader.WebappClassLoaderBase.checkThreadLocalMapForLeaks The web application [ROOT] created a ThreadLocal with key of type [java.lang.ThreadLocal] (value [java.lang.ThreadLocal@2e95a705]) and a value of type [org.restlet.Context] (value [org.restlet.Context@7c1769c3]) but failed to remove it when the web application was stopped. Threads are going to be renewed over time to try and avoid a probable memory leak.
25-May-2023 14:40:54.733 SEVERE [Thread-40] org.apache.catalina.loader.WebappClassLoaderBase.checkThreadLocalMapForLeaks The web application [ROOT] created a ThreadLocal with key of type [java.lang.ThreadLocal] (value [java.lang.ThreadLocal@661ecba3]) and a value of type [org.restlet.data.Response] (value [org.restlet.data.Response@6450ab0c]) but failed to remove it when the web application was stopped. Threads are going to be renewed over time to try and avoid a probable memory leak.
25-May-2023 14:40:54.769 INFO [Thread-40] org.apache.coyote.AbstractProtocol.stop Stopping ProtocolHandler ["http-nio-8080"]
25-May-2023 14:40:55.791 INFO [Thread-40] org.apache.coyote.AbstractProtocol.destroy Destroying ProtocolHandler ["http-nio-8080"]
```

I was watching the stream of kubernetes events at the same time. The startup probe was still failing, we hadn't gotten a success event from that, but suddenly we switched to the liveness probe which also failed. But around the same time the readiness probe also failed. I think one of those caused it to be killed. (These logs come from `kubectl get events -nxnat --watch -o yaml`)
```
---
apiVersion: v1
count: 2
eventTime: null
firstTimestamp: "2023-05-25T14:34:40Z"
involvedObject:
  apiVersion: v1
  fieldPath: spec.containers{xnat-web}
  kind: Pod
  name: xnat-xnat-web-0
  namespace: xnat
  resourceVersion: "6788"
  uid: d568ae26-3023-48a7-ba87-5c30cd18d2bf
kind: Event
lastTimestamp: "2023-05-25T14:40:56Z"
message: Created container xnat-web
metadata:
  creationTimestamp: "2023-05-25T14:34:40Z"
  name: xnat-xnat-web-0.176269908aca510a
  namespace: xnat
  resourceVersion: "6949"
  uid: 1f3851bd-f212-4816-a66e-6e66afd3a127
reason: Created
reportingComponent: ""
reportingInstance: ""
source:
  component: kubelet
  host: colima
type: Normal
---
apiVersion: v1
count: 2
eventTime: null
firstTimestamp: "2023-05-25T14:34:40Z"
involvedObject:
  apiVersion: v1
  fieldPath: spec.containers{xnat-web}
  kind: Pod
  name: xnat-xnat-web-0
  namespace: xnat
  resourceVersion: "6788"
  uid: d568ae26-3023-48a7-ba87-5c30cd18d2bf
kind: Event
lastTimestamp: "2023-05-25T14:40:56Z"
message: Started container xnat-web
metadata:
  creationTimestamp: "2023-05-25T14:34:40Z"
  name: xnat-xnat-web-0.176269909456925e
  namespace: xnat
  resourceVersion: "6950"
  uid: 8b50310f-576d-4172-8562-da563c5f1799
reason: Started
reportingComponent: ""
reportingInstance: ""
source:
  component: kubelet
  host: colima
type: Normal
---
apiVersion: v1
count: 10
eventTime: null
firstTimestamp: "2023-05-25T14:35:34Z"
involvedObject:
  apiVersion: v1
  fieldPath: spec.containers{xnat-web}
  kind: Pod
  name: xnat-xnat-web-0
  namespace: xnat
  resourceVersion: "6788"
  uid: d568ae26-3023-48a7-ba87-5c30cd18d2bf
kind: Event
lastTimestamp: "2023-05-25T14:45:34Z"
message: 'Startup probe failed: Get "http://10.42.0.10:8080/app/template/Login.vm#!":
  context deadline exceeded (Client.Timeout exceeded while awaiting headers)'
metadata:
  creationTimestamp: "2023-05-25T14:35:34Z"
  name: xnat-xnat-web-0.1762699d3a015466
  namespace: xnat
  resourceVersion: "7046"
  uid: fcbd4cd7-d2b7-42a4-8b04-6bf178be4155
reason: Unhealthy
reportingComponent: ""
reportingInstance: ""
source:
  component: kubelet
  host: colima
type: Warning
---
apiVersion: v1
count: 2
eventTime: null
firstTimestamp: "2023-05-25T14:40:48Z"
involvedObject:
  apiVersion: v1
  fieldPath: spec.containers{xnat-web}
  kind: Pod
  name: xnat-xnat-web-0
  namespace: xnat
  resourceVersion: "6788"
  uid: d568ae26-3023-48a7-ba87-5c30cd18d2bf
kind: Event
lastTimestamp: "2023-05-25T14:46:58Z"
message: 'Liveness probe failed: Get "http://10.42.0.10:8080/app/template/Login.vm#!":
  context deadline exceeded (Client.Timeout exceeded while awaiting headers)'
metadata:
  creationTimestamp: "2023-05-25T14:40:48Z"
  name: xnat-xnat-web-0.176269e6668ac160
  namespace: xnat
  resourceVersion: "7082"
  uid: 5ba7eb7b-0adb-4007-af99-febf45a3c484
reason: Unhealthy
reportingComponent: ""
reportingInstance: ""
source:
  component: kubelet
  host: colima
type: Warning
---
apiVersion: v1
count: 2
eventTime: null
firstTimestamp: "2023-05-25T14:40:48Z"
involvedObject:
  apiVersion: v1
  fieldPath: spec.containers{xnat-web}
  kind: Pod
  name: xnat-xnat-web-0
  namespace: xnat
  resourceVersion: "6788"
  uid: d568ae26-3023-48a7-ba87-5c30cd18d2bf
kind: Event
lastTimestamp: "2023-05-25T14:46:58Z"
message: 'Readiness probe failed: Get "http://10.42.0.10:8080/app/template/Login.vm#!":
  context deadline exceeded (Client.Timeout exceeded while awaiting headers)'
metadata:
  creationTimestamp: "2023-05-25T14:40:48Z"
  name: xnat-xnat-web-0.176269e6668b3eb3
  namespace: xnat
  resourceVersion: "7083"
  uid: f8b68268-f479-41b4-9edb-c6685925c01b
reason: Unhealthy
reportingComponent: ""
reportingInstance: ""
source:
  component: kubelet
  host: colima
type: Warning
---
apiVersion: v1
count: 2
eventTime: null
firstTimestamp: "2023-05-25T14:40:48Z"
involvedObject:
  apiVersion: v1
  fieldPath: spec.containers{xnat-web}
  kind: Pod
  name: xnat-xnat-web-0
  namespace: xnat
  resourceVersion: "6788"
  uid: d568ae26-3023-48a7-ba87-5c30cd18d2bf
kind: Event
lastTimestamp: "2023-05-25T14:46:58Z"
message: Container xnat-web failed liveness probe, will be restarted
metadata:
  creationTimestamp: "2023-05-25T14:40:48Z"
  name: xnat-xnat-web-0.176269e666a9308d
  namespace: xnat
  resourceVersion: "7084"
  uid: cc1d7e5b-808a-438c-a84a-6bd08934bb0d
reason: Killing
reportingComponent: ""
reportingInstance: ""
source:
  component: kubelet
  host: colima
type: Normal
```
Looks like the liveness probe is the one that did it. I'll extend that time / number of failed checks and see if that helps.

## Results
We have an application! I was able to get it started, log in, all that jazz! I tried to log in immediately upon startup finishing, thinking that maybe the reason it was getting killed by the probes is that no one had completed the first login. Well, I logged in and it wasn't killed, so that at least doesn't disprove the hypothesis.

I think the limiting factor before was CPU, not memory. The configuration that worked to get things started:

- VM: `colima start --cpu 6 --memory 16 --kubernetes`
- App in helm chart
    - Requests: CPU 4, memory 8 Gi
    - Limits: CPU 6, memory 12 Gi

# New Docker Image
2023-05-31

Startup wasn't consistent. Sometimes it would start, sometimes not after 20 minutes. Had a long conversation about it on slack. We arrived at the idea that maybe the AIS docker image was to blame, and I could whip up a new one and test it out.

I did that. I cobbled together a dockerfile from the [AIS build](https://github.com/Australian-Imaging-Service/xnat-docker-build/tree/main) and the [Radiologics xnat-local build](https://bitbucket.org/radiologics/radiologics-xnat-docker/src/master/xnat-local/tomcat/dockerfile).

It worked incredibly well. Or, at least, it improved startup time by more than an order of magnitude. Previously the startup might get killed because it hadn't completed after 20 minutes. This image started up in <2 minutes.
```
31-May-2023 19:50:58.277 INFO [main] org.apache.catalina.startup.Catalina.start Server startup in [74805] milliseconds
```

It isn't perfect. For instance, I had tried to write a site config file to set the initial values and paths and whatever. That only worked partially. Could be fixed.

But still, this is way better than what we had before.

# Container Service
## 2023-05-31

Had trouble getting container service working. Site started up and there was no plugin. It was in the image,
but it didn't make it through the init container copying it into a mount.

Figured out the issue. The init container expected the name of all the plugin jars will be `{plugin_name}-*`.
I hadn't named my CS jar that way.

Changed the name and it worked. ...at least, it was pulled into the main image's volume and detected at runtime.

Next issue is that it couldn't connect to docker (because there is no socket) and couldn't connect to kubernetes (because we didn't make the kubeconfig file CS needs).

This will take some modifications to the helm chart.

## 2023-06-07
I went through a few iterations but I got a set of chart modifications that let the container service run.

I spent a lot of time trying to follow my own instructions from [Container Service on Kubernetes: Setup](https://bitbucket.org/xnatdev/container-service/src/master/docs/kubernetes/container-service-on-kubernetes-setup.md).
XNAT is running in one namespace with one service account, while CS launches containers in another namespace with another service account. 
This turned out to be very hard to do! Not creating the namespaces and accounts, that part was fine.
But I could not figure out how to gather the credentials that the container service needs and provide them to the XNAT pod. I had to have some way to run `kubectl` commands to get the container service's credentials and write them into a file.

# Skaffold (2022)
The `Dockerfile` and `skaffold.yaml` in this repo were what I used for my testing.

## Results
They were pretty mixed! I never got things working 100%.

One reason is architecture. I'm on an M1 mac. That means I can't use VirtualBox to host my Kubernetes, I have to use docker desktop. (With the caveat that I also tried some [lima](https://github.com/lima-vm/lima) VM configurations but I couldn't make things work with those either.) That had a few knock-on effects in how things could run.

Sometimes, depending on settings, the app would start up but would stall or hang. That seemed to point to resource issues. By then end I was throwing basically all of my machine's resources at docker just to get anything to run. But it seems my machine is not beefy enough to be running the kubernetes cluster and containers that are required to orchestrate everything. 

Another symptom we would see intermittently were crashes. And not ones that would produce nice java stack traces. These would look something like this:

    14-Feb-2022 18:59:10.304 INFO [main] org.apache.catalina.core.AprLifecycleListener.initializeSSL OpenSSL successfully initialized [OpenSSL 1.1.1d  10 Sep 2019]
    14-Feb-2022 18:59:12.039 INFO [main] org.apache.coyote.AbstractProtocol.init Initializing ProtocolHandler ["http-nio-8080"]
    #
    # A fatal error has been detected by the Java Runtime Environment:
    #
    #  SIGSEGV (0xb) at pc=0x000000400b877ca0, pid=1, tid=0x00000040030d2700
    #
    # JRE version: OpenJDK Runtime Environment (8.0_292-b10) (build 1.8.0_292-b10)
    # Java VM: OpenJDK 64-Bit Server VM (25.292-b10 mixed mode linux-amd64 compressed oops)
    # Problematic frame:
    # J 50 C1 java.lang.Class.getName()Ljava/lang/String; (21 bytes) @ 0x000000400b877ca0 [0x000000400b877c80+0x20]
    #
    # Failed to write core dump. Core dumps have been disabled. To enable core dumping, try "ulimit -c unlimited" before starting Java again
    #
    # An error report file with more information is saved as:
    # /usr/local/tomcat/hs_err_pid1.log
    #
    # If you would like to submit a bug report, please visit:
    #   http://bugreport.java.com/bugreport/crash.jsp
    #
    qemu: uncaught target signal 6 (Aborted) - core dumped

My guess is these were caused by some kind of mismatch in the instructions between the architecture within the image and outside on my native hardware.

The core problem, and the one that I feel really demonstrates the non-vialibity of this for development right now, is the slow startup time. In the instances were everything was configured properly and XNAT did eventually start, startup would take around 5 minutes. That's too long to wait and kills development velocity.

Plus, in the last few experiments I tried, XNAT would start only for me to realize that something about my configuration meant it didn't copy in my container service jar at all. So the whole startup time was for nothing. 😬

## Install
The way I wrote the `Dockerfile` and `skaffold.yaml` expects that you'll have some local files in particular places. Sorry about that; I was trying to go fast and make a proof of concept, not make something good.

You need to have the [`container-service`](https://bitbucket.org/xnatdev/container-service) repo cloned, and the fat jar built by running `./gradlew fatJar`. (And that needs to run with Java 8.) Or you can download a release jar, for example [`container-service-3.1.0-fat.jar`](https://bitbucket.org/xnatdev/container-service/downloads/container-service-3.1.0-fat.jar), and put it into a local `build/libs/` directory. That's where the `Dockerfile` expects to see it.

Next you need to have the [AIS/charts](https://github.com/Australian-Imaging-Service/charts) repo cloned in a sibling directory. The `skaffold.yaml` expects to read it at `../charts`.