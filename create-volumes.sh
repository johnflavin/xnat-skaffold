#!/bin/bash

# Create archive and logs dirs if they don't already exist
mkdir archive logs &2>/dev/null

# Create archive and logs PVs and PVCs
kubectl apply -f - << EOF
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: archive
spec:
  capacity:
    storage: 250Mi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: ${PWD}/archive
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: logs
spec:
  capacity:
    storage: 100Mi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: ${PWD}/logs
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: archive
  namespace: xnat
spec:
  storageClassName: "" # Empty string must be explicitly set otherwise default StorageClass will be set
  volumeName: archive
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 250Mi
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: logs
  namespace: xnat
spec:
  storageClassName: "" # Empty string must be explicitly set otherwise default StorageClass will be set
  volumeName: logs
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 100Mi
EOF
