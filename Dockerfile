FROM tomcat:9.0.75-jdk8-corretto-al2

ARG XNAT_ROOT=/data/xnat
ARG XNAT_HOME=${XNAT_ROOT}/home
ARG ARCHIVE_DIR=${XNAT_ROOT}/archive
ARG PREARCHIVE_DIR=${XNAT_ROOT}/prearchive
ARG BUILD_DIR=${XNAT_ROOT}/build
ARG CACHE_DIR=${XNAT_ROOT}/cache
ARG PIPELINE_DIR=${XNAT_ROOT}/pipeline
ARG CONFIG_DIR=${XNAT_HOME}/config

ARG XNAT_SMTP_ENABLED=false
ARG TOMCAT_XNAT_FOLDER=ROOT
ARG TOMCAT_XNAT_FOLDER_PATH=${CATALINA_HOME}/webapps/${TOMCAT_XNAT_FOLDER}

ARG SITE_URL=http://localhost:8080
ARG SITE_ID=XNAT
ARG ADMIN_EMAIL=admin@xnat.org

# default environment variables
ENV XNAT_HOME=${XNAT_HOME} \
    CONFIG_DIR=${CONFIG_DIR} \
    SITE_URL=${SITE_URL} \
    SITE_ID=${SITE_ID} \
    ADMIN_EMAIL=${ADMIN_EMAIL} \
    ARCHIVE_PATH=${ARCHIVE_DIR} \
    BUILD_PATH=${BUILD_DIR} \
    CACHE_PATH=${CACHE_DIR} \
    PIPELINE_PATH=${PIPELINE_DIR} \
    PREARCHIVE_PATH=${PREARCHIVE_DIR}

# RUN apt-get update && apt-get install -y postgresql-client

RUN rm -rf ${CATALINA_HOME}/webapps/*
RUN mkdir -p \
        ${TOMCAT_XNAT_FOLDER_PATH} \
        ${CONFIG_DIR} \
        ${XNAT_HOME}/logs \
        ${XNAT_HOME}/work \
        ${ARCHIVE_DIR} \
        ${BUILD_DIR} \
        ${CACHE_DIR} \
        ${PIPELINE_DIR} \
        ${PREARCHIVE_DIR}

COPY xnat-config/make-prefs-init-ini.sh ${CONFIG_DIR}
RUN ${CONFIG_DIR}/make-prefs-init-ini.sh

COPY xnat-config/executor.properties ${CONFIG_DIR}

# Copy exploded XNAT war
COPY xnat-exploded/ ${TOMCAT_XNAT_FOLDER_PATH}

# Copy plugins from local files
COPY plugins/ ${XNAT_HOME}/plugins

# ADD lib/libclib_jiio.so /usr/lib/jvm/java-8-oracle/jre/lib/amd64/

ADD xnat-config/xnat.xml /usr/local/tomcat/conf/Catalina/localhost/ROOT.xml

WORKDIR /usr/local/tomcat
CMD ["catalina.sh", "jpda", "run"]
USER 0
